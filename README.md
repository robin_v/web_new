# web_new

> 用vue开发的黄金网站，

## 功能模块

登录、注册、首页、列表、详情、购物车、个人资产、更多

## 项目部署

该项目是单页面应用，需要借助nginx部署,nginx部分代码如下:
```nginx
server {
        listen       9888;
        server_name  localhost;

        #charset koi8-r;

        #access_log  logs/host.access.log  main;
        root /var/www/web/;
        index index.html;
        location / {
            try_files $uri $uri/ @rewrites;
        }
        location @rewrites {
            rewrite ^(.+)$ /index.html last;
         }

  	location ~* \.(?:ico|css|js|gif|jpe?g|png)$ {
    	 # Some basic cache-control for static files to be sent to the browser
         expires max;
         add_header Pragma public;
         add_header Cache-Control "public, must-revalidate, proxy-revalidate";
         }
	
    }
```

## 项目截图

![首页](img1.png)

![详情](img2.png)

## 项目存在问题

- 未能使用vux进行统一状态管理
- 项目结构、组件分类不明确

## 总结
vue确实是一个很不错的选择，组件式开发使得开发效率大幅度提升