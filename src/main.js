// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import VueLazyload from 'vue-lazyload'

import App from './App'
import routes from './router'
import jQuery from 'jquery'
import bootstrap from 'bootstrap'
import loaddingBase64 from './loaddingBase64'
import VueCookie from 'vue-cookie'

/* eslint-disable no-new */
Vue.use(VueRouter)
Vue.use(VueResource)
Vue.use(VueCookie)
Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: loaddingBase64.loading,
  loading: loaddingBase64.loading,
  attempt: 1
})
Vue.http.options.emulateJSON = true
const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: routes
})
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {App, jQuery, bootstrap}
})
