/**
 * Created by ren on 2016/12/26.
 */
/**
 *
 * @type {string}
 * http://114.215.84.189:8889/goldxianshang/api/
 * nginx http://114.215.84.189:9798/goldxianshang/api/
 * http://www.51aujia.com/goldxianshang/api/
 */
const rootPath = 'http://114.215.84.189:9798/goldxianshang/api/'
const API = {
  imgcodeUrl: 'http://www.51aujia.com:8889/',
  path: 'http://www.51aujia.com:8889/goldxianshang',
  login: rootPath + 'user/login',
  valiCode: rootPath + 'user/getValiCode',
  getCYSmsCode: rootPath + 'user/getCYSmsCode',
  register: rootPath + 'user/register',
  forgetPwd: rootPath + 'user/forgetPwd',
  findBanners: rootPath + 'banner/findBanners',
  findOnePage: rootPath + 'kinsonGold/findOnePage',
  findRegularPutin: rootPath + 'regularPutin/findRegularPutin',
  findKinsonGoldList: rootPath + 'kinsonGold/findKinsonGoldList',
  findGoldPrice: rootPath + 'user/findGoldPrice',
  findGoldshoppingList: rootPath + 'goldshopping/findGoldshoppingList',
  findGoldshoppingById: rootPath + 'goldshopping/findGoldshoppingById',
  findCommentList: rootPath + 'comment/findCommentList',
  saveAddress: rootPath + 'address/saveAddress',
  addCar: rootPath + 'shopCar/addCar',
  ShopCarList: rootPath + 'shopCar/ShopCarList',
  findMyAssets: rootPath + 'user/findMyAssets',
  getShopPrice: rootPath + 'order/getShopPrice',
  findAddressList: rootPath + 'address/findAddressList',
  saveOrderOne: rootPath + 'order/saveOrder',
  wapPay: rootPath + 'unionpay/wapPay',
  accountPay: rootPath + 'pay/accountPay',
  addAccount: rootPath + 'user/addAccount',
  findMyBankList: rootPath + 'myBank/findMyBankList',
  extractCash: rootPath + 'chinapay/extractCash',
  findGoldBeanDetailList: rootPath + 'goldBeanDetail/findGoldBeanDetailList',
  findTradeDetail: rootPath + 'user/findTradeDetail',
  kinsonSaveOrder: rootPath + 'order/kinsonSaveOrder',
  shoppingRecord: rootPath + 'buyRecord/shoppingRecord',
  cancelOrder: rootPath + 'order/cancelOrder',
  getRouteRequest: rootPath + 'pay/getRouteRequest',
  deleteShopCars: rootPath + 'shopCar/deleteShopCars',
  MyredvelopeVo: rootPath + 'mycoupons/MyredvelopeVo',
  findMycouponsList: rootPath + 'mycoupons/findMycouponsList',
  updateAddress: rootPath + 'address/updateAddress',
  operateAddressById: rootPath + 'address/operateAddressById',
  getBankName: rootPath + 'myBank/getBankName',
  saveMyBank: rootPath + 'myBank/saveMyBank',
  deleteMyBank: rootPath + 'myBank/deleteMyBank',
  saveFeedBack: rootPath + 'feedBack/saveFeedBack',
  KinsonBuyRecord: rootPath + 'buyRecord/KinsonBuyRecord',
  KinsonRenew: rootPath + 'buyRecord/KinsonRenew',
  updatePaymentPassword: rootPath + 'user/updatePaymentPassword',
  setPaymentPassword: rootPath + 'user/setPaymentPassword',
  regularPutInRecord: rootPath + 'buyRecord/regularPutInRecord',
  extractRecord: rootPath + 'extractRecord/extractRecord',
  saveRedvelope: rootPath + 'mycoupons/saveRedvelope',
  saveOrder: rootPath + 'regularPutin/saveOrder',
  saveComment: rootPath + 'comment/saveComment',
  saveDtOrder: rootPath + 'extractOrder/saveDtOrder',
  findAddressByOrderNo: rootPath + 'address/findAddressByOrderNo',
  updateShopCars: rootPath + 'shopCar/updateShopCars',
  saveJSJOrder: rootPath + 'extractOrder/saveJSJOrder',
  consigned: rootPath + 'confirmRevice/consigned'
}
export default API
