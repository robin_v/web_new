/**
 * Created by ren on 2016/12/26.
 */
'use strict'
import Login from './Login'
import Home from './Home'
import Register from './Register'
import RorgetPwd from './ForgetPwd'
import JSJDetail from './JSJDetail'
import OrderConfirm from './components/order/OrderConfirm'
import GoodsList from './GoodsList'
import GoodsDetail from './GoodsDetail'
import OrderAddress from './components/order/OrderAddress'
import CartList from './components/CartList'
import OrderAccount from './components/order/OrderAccount'
import OrderSingleAccount from './components/order/OrderSingleAccount'
import PaySuccess from './components/pay/PaySuccess'
import DingTouDetail from './DingTouDetail'
import PersonMain from './PersonMain'
import Asset from './components/person/Asset'
import Loose from './components/person/Loose'
import MyGold from './components/person/MyGold'
import More from './More'
import MoreGoods from './components/more/MoreGoods'
import Comment from './components/more/Comment'
import Track from './components/more/Track'
import DTDetail from './components/more/DTDetail'
import MoreJSJDetail from './components/more/JSJDetail'
import RedpackList from './components/more/RedpackList'
import Voucher from './components/more/Voucher'
import SafeMain from './components/more/SafeMain'
import Opinions from './components/more/Opinions'
import Help from './Help'
import UpdateOrderAddress from './components/order/UpdateOrderAddress'
import XCList from './components/more/XCList'
import TQList from './components/more/TQList'
import MoreSuccess from './components/more/MoreSuccess'
import MoreOrderAccount from './components/more/MoreOrderAccount'
import MoreGoodsDetail from './components/more/MoreGoodsDetail'
import MoreOrderConfirm from './components/more/MoreOrderConfirm'
import AboutAu from './AboutAu+'
import Share from './components/Share'
import MoreShare from './components/more/MoreShare'
import MorezcJsj from './components/more/MorezcJsj'
import DingtouXcList from './components/more/DingtouXcList'
import MoreDingTouDetail from './components/more/DingTouDetail'
import MoreDtOrderAccount from './components/more/MoreDtOrderAccount'
import MoreDTGoodsDetail from './components/more/MoreDTGoodsDetail'
import NewShow from './NewShow'
const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/newShow',
    name: 'newShow',
    component: NewShow
  },
  {
    path: '/help',
    name: 'help',
    component: Help
  },
  {
    path: '/aboutAu',
    name: 'aboutAu',
    component: AboutAu
  },
  {
    path: '/share/:orderNo',
    name: 'share',
    component: Share
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/forgetPwd',
    name: 'forgetPwd',
    component: RorgetPwd
  },
  {
    path: '/orderConfirm/:style',
    name: 'orderConfirm',
    component: OrderConfirm
  },
  {
    path: '/goodsdetail/:goodsId',
    name: 'goodsdetail',
    component: GoodsDetail
  },
  {
    path: '/jSJDetail/:jsjId',
    name: 'jSJDetail',
    component: JSJDetail
  },
  {
    path: '/goodsList',
    name: 'goodsList',
    component: GoodsList
  },
  {
    path: '/orderAddress',
    name: 'orderAddress',
    component: OrderAddress
  },
  {
    path: '/updateOrderAddress',
    name: 'updateOrderAddress',
    component: UpdateOrderAddress
  },
  {
    path: '/cartList',
    name: 'cartList',
    component: CartList
  },
  {
    path: '/orderAccount',
    name: 'orderAccount',
    component: OrderAccount
  },
  {
    path: '/orderSingleAccount',
    name: 'orderSingleAccount',
    component: OrderSingleAccount
  },
  {
    path: '/paySuccess',
    name: 'paySuccess',
    component: PaySuccess
  },
  {
    path: '/dingTouDetail',
    name: 'dingTouDetail',
    component: DingTouDetail
  },
  {
    path: '/personMain',
    name: 'personMain',
    component: PersonMain,
    children: [
      {
        path: 'asset',
        name: 'asset',
        component: Asset
      },
      {
        path: 'loose',
        name: 'loose',
        component: Loose
      },
      {
        path: 'myGold',
        name: 'myGold',
        component: MyGold
      }
    ]
  },
  {
    path: '/more',
    name: 'more',
    component: More,
    children: [
      {
        path: 'moreGoods',
        name: 'moreGoods',
        component: MoreGoods
      },
      {
        path: 'comment',
        name: 'comment',
        component: Comment
      },
      {
        path: 'track/:orderNo',
        name: 'track',
        component: Track
      },
      {
        path: 'dtDetail/:dtId',
        name: 'dtDetail',
        component: DTDetail
      },
      {
        path: 'moreDingTouDetail',
        name: 'moreDingTouDetail',
        component: MoreDingTouDetail
      },
      {
        path: 'moreJSJDetail/:jsjId',
        name: 'moreJSJDetail',
        component: MoreJSJDetail
      },
      {
        path: 'redpackList',
        name: 'redpackList',
        component: RedpackList
      },
      {
        path: 'voucher',
        name: 'voucher',
        component: Voucher
      },
      {
        path: 'safeMain',
        name: 'safeMain',
        component: SafeMain
      },
      {
        path: 'opinions',
        name: 'opinions',
        component: Opinions
      },
      {
        path: 'xcList/:jsjId',
        name: 'xcList',
        component: XCList
      },
      {
        path: 'dingtouXcList/:jsjId',
        name: 'dingtouXcList',
        component: DingtouXcList
      },
      {
        path: 'tqList',
        name: 'tqList',
        component: TQList
      },
      {
        path: 'moreSuccess',
        name: 'moreSuccess',
        component: MoreSuccess
      },
      {
        path: 'moreOrderAccount',
        name: 'moreOrderAccount',
        component: MoreOrderAccount
      },
      {
        path: 'moreDtOrderAccount',
        name: 'moreDtOrderAccount',
        component: MoreDtOrderAccount
      },
      {
        path: 'moreGoodsDetail/:goodsId',
        name: 'moreGoodsDetail',
        component: MoreGoodsDetail
      },
      {
        path: 'moreDTGoodsDetail/:goodsId',
        name: 'moreDTGoodsDetail',
        component: MoreDTGoodsDetail
      },
      {
        path: 'moreOrderConfirm/:style',
        name: 'moreOrderConfirm',
        component: MoreOrderConfirm
      },
      {
        path: 'morezcJsj',
        name: 'morezcJsj',
        component: MorezcJsj
      },
      {
        path: 'moreShare/:orderNo',
        name: 'moreShare',
        component: MoreShare
      }
    ]
  }
]
export default routes

